package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMappingAndValueTypeTest extends JpaTestBase {
    @Autowired
    CompanyProfileRepository companyProfileRepository;
    @Autowired
    UserProfileRepository userProfileRepository;

    @Test
    void should_save_company_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            CompanyProfile companyProfile = companyProfileRepository
                    .save(new CompanyProfile("city", "street"));
            expectedId.setValue(companyProfile.getId());
        });

        run(em -> {
            CompanyProfile companyProfile = companyProfileRepository
                    .findById(expectedId.getValue())
                    .orElseThrow(NoSuchElementException::new);
            assertEquals("city", companyProfile.getCity());
            assertEquals("street", companyProfile.getStreet());
        });
    }

    @Test
    void should_save_user_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(em -> {
            UserProfile userProfile = userProfileRepository
                    .save(new UserProfile("hello", "world"));
            expectedId.setValue(userProfile.getId());
        });

        run(em -> {
            UserProfile userProfile = userProfileRepository
                    .findById(expectedId.getValue())
                    .orElseThrow(NoSuchElementException::new);
            assertEquals("hello", userProfile.getAddressCity());
            assertEquals("world", userProfile.getAddressStreet());
        });
    }
}
